title: Quelques questions pour découvrir Mobilizon…
clic: (cliquez sur les questions pour découvrir les réponses)
prez:
  title: Présentation de Mobilizon
  what:
    q: C’est quoi Mobilizon ?
    a:
      p:
      - C’est un logiciel qui, une fois installé sur un serveur, permettra de créer
        un site web sur lequel des personnes pourront créer des événements, à la
        manière des événements Facebook ou Meetup.
      - Installer Mobilizon permettra à des collectifs de s’émanciper des outils
        des géants du web en créant leur propre plateforme d’événements. Cette installation
        (appelée « instance ») pourra s’interconnecter simplement avec d’autres,
        grâce à <a href="https://framablog.org/2019/03/07/la-fee-diverse-deploie-ses-ailes/">un
        protocole de fédération décentralisée</a>.
      - Même s’il pourra être utilisé pour se rassembler autour d’un goûter d’anniversaire
        ou d’une compétition de karaté, Mobilizon sera développé pour répondre aux
        militant·es, qui ont besoin d’outils spécifiques pour s’organiser de manière
        libre et durable.
  facebook:
    q: Mobilizon ne sera pas Facebook
    a:
      p:
      - Dans sa conception, Mobilizon n’est pas pensé pour « être le Facebook killer ».
      - Nous voyons bien le danger qu’il y a à publier un rassemblement militant
        sur Facebook, une plateforme monopolistique qui <a href=" https://dayssincelastfacebookscandal.com/">multiplie
        les scandales</a> concernant la vie privée et la manipulation de l’opinion
        publique. Seulement, comparés à ceux des géants du web, nos moyens sont
        modestes.
      - 'Notre ambition l’est tout autant : commençons par un outil qui fait peu
        mais bien. Un outil construit sur une base solide qui permettra ensuite
        de proposer davantage de fonctionnalités. Nous avons fait le choix de nous
        concentrer sur les besoins spécifiques d’un public particulier (le public
        militant), ce qui n’empêchera pas d’autres communautés d’utiliser Mobilizon
        dans d’autres cas. Ensuite, à plus long terme, nous serons en mesure d’adapter
        l’outil à ces autres publics.'
      - Produire Mobilizon, ce n’est donc pas un sprint, ou l’on promet tout, tout
        de suite, à tout le monde. C’est plus une course de fond, où la première
        étape est de développer un outil qui permettra de créer des événements et
        de le faire bien.
  features:
    q: Est-ce que Mobilizon me permettra de…?
    a:
      p:
      - 'Créer des événements récurrents ? Partager mes événements sur Twitter ?
        Envoyer des messages privés ? etc ? les idées et envies de fonctionnalités
        sont aussi multiples que nous sommes enthousiastes… Seulement voilà, la
        réponse est forcément :'
      - Ça dépend ! 😃
      - La liste de fonctionnalités pour la v1 dépendra du montant de la collecte.
      - Et ce qui adviendra après la v1 dépendra de la communauté Mobilizon.
      - Si le succès de cette collecte dépend plus de vous que de nous, de notre
        côté, nous pouvons promettre que nous informerons des avancées de Mobilizon
        à chaque nouvelle étape franchie, sur <a href="@:link.joinmobilizon@:lang/news">la
        page des actualités de ce site</a> et sur la newsletter dédiée.
  advantages:
    q: Quels sont les 3 avantages de Mobilizon ?
    a:
      p:
      - '<b>Libre</b> : la licence de Mobilizon garantit le respect des libertés
        fondamentales des humains qui l’utiliseront. Son <a href="https://framagit.org/framasoft/mobilizon/">code
        source étant publié</a>, il est publiquement auditable, ce qui garantit
        sa transparence. Si la direction donnée par l’équipe de développement ne
        vous convient pas, vous avez légalement le droit de créer votre version
        du logiciel avec vos propres choix de gouvernance.'
      - '<b>Éthique</b> :  Mobilizon  est développé sans but lucratif. Il n’y aura
        donc aucun mécanisme de profilage, ni de captation de l’attention. Au contraire,
        Mobilizon est pensé pour rendre un maximum de pouvoir aux personnes qui
        l’utilisent.'
      - '<b>Humain</b> : Moblizon n’est pas développé par une start-up inaccessible,
        mais par un groupe d’ami·e·s qui s’évertuent à <a href="https://framasoft.org">changer
        le monde, un octet à la fois</a>. Alors, certes, on va moins vite, mais
        on reste à l’écoute et dans l’échange. D’ailleurs, Mobilizon a été conçu
        en interrogeant des militant·es sur leurs usages numériques.'
  federated:
    q: Pourquoi c’est mieux que ce soit un outil fédéré ?
    a:
      p:
      - 'Imaginons que mon université crée son instance <em>MobilizTaFac</em> d’un
        côté, et que mon mouvement pour le climat crée son instance <em>ÉcoMobilizés</em>
        de l’autre : est-ce que je dois créer un compte sur chaque site, histoire
        de me tenir au courant des rassemblements prévus ?'
      - 'Non : ce serait, selon nous, un gros frein à l’usage. C’est pour cela que
        nous souhaitons que Mobilizon soit fédéré : chaque instance (site de publication
        d’événements) propulsée par le logiciel Mobilizon pourra alors choisir d’échanger
        avec d’autres instances, d’afficher plus d’événements que « juste les siens »,
        et de favoriser les interactions.'
      - Le protocole de fédération, par le standard le plus répandu (nommé <a href="https://fr.wikipedia.org/wiki/ActivityPub">ActivityPub</a>),
        permettra en plus, à terme, de tisser des ponts avec <a href="https://joinmastodon.org">Mastodon</a>
        (l’alternative libre et fédérée à Twitter), <a href="https://joinpeertube.org">PeerTube</a>
        (alternative libre et fédérée à YouTube), et bien d’autres outils alternatifs.
  install:
    q: Comment installer Mobilizon sur un serveur ?
    a:
      p:
      - Pour l’instant, ce n’est pas encore possible, le développement étant en
        cours, rien n’est fait pour rendre l’installation facile.
      - Il faudra attendre la sortie de la version bêta (automne 2019) et de la
        version 1 (premier semestre 2020) pour avoir des installations facilitées
        par une documentation complète, voire des packaging.
      - Cependant, si vous voulez suivre l’évolution du développement, <a href="https://framagit.org/framasoft/mobilizon">le
        code est là</a>.
  develop:
    q: Comment participer au code de Mobilizon ?
    a:
      p:
      - Tout d’abord, il va falloir des connaissances en Git et en Elixir. Si cela
        ne vous dit rien, c’est que, pour l’instant, le projet n’est pas en capacité  de
        recevoir vos contributions en code.
      - Ensuite, il suffit d’aller sur <a  rel="noopener noreferrer" target="_blank"
        href="https://framagit.org/framasoft/mobilizon/">le dépôt du logiciel</a>,
        et d’écrire une issue ou bien de le forker pour commencer à proposer ses
        propres contributions.
      - Bien entendu, c’est toujours mieux de venir se parler avant, par exemple
        en utilisant <a target="_blank" rel="noopener noreferrer" href="https://riot.im/app/#/room/#Mobilizon:matrix.org">notre
        salon Matrix</a>.
  contribute:
    q: Comment participer à Mobilizon, si je ne sais pas coder ?
    a:
      p:
      - Le plus simple, c’est de venir parler avec nous, sur <a target="_blank"
        rel="noopener noreferrer" href="https://framacolibri.org/c/mobilizon/fr-francais">l’espace
        dédié à Mobilizon</a> dans notre forum des contributions.
      - Souvenez-nous que nous ne sommes pas une multi-nationale du web, ni même
        une grosse start-up, mais une association de moins de 40 membres (qui mènent
        d’autres projets en parallèle) . Ne vous vexez donc pas si nous mettons
        du temps  à vous répondre !
      - Vous avez probablement des idées formidables à apporter au projet, et merci
        à vous si vous prenez le temps de les proposer. Cependant, nous sommes en
        mesure d’assurer ce que nous avons promis de faire pour la version bêta
        et la V1, mais nous savons aussi que nous ne pourrons alourdir ce programme
        déjà bien chargé.
      - Ainsi, il faut s’attendre à ce que vos envies et propositions ne soient
        pas traitées avant les développements postérieurs à la version 1, que nous
        (Framasoft) espérons avoir les moyens de pouvoir mener (mais cela dépendra
        de la réussite de cette collecte !).
  cost:
    q: Combien ça va me coûter d’utiliser Mobilizon ?
    a:
      p:
      - Le but de financer ce logiciel dès sa production c’est qu’ensuite, un grand
        nombre d’organisations puissent en proposer un usage gratuit, comme cela
        se fait pour la plupart des instances <a href="https://joinmastodon.org">Mastodon</a>
        (l’alternative libre et fédérée à Twitter) et <a href="https://joinpeertube.org">PeerTube</a>
        (alternative libre et fédérée à YouTube).
      - Ce qui est certain, c’est que le logiciel Mobilizon sera diffusé gratuitement
        et que personne n’aura rien à nous payer pour pouvoir l’installer sur son
        serveur (et qu’on essaiera même de vous faciliter la vie).
      - Ce que nous espérons, c’est que l’enthousiasme autour de ce service nous
        donne les moyens d’ouvrir une instance, publique et gratuite, lors de la
        sortie de la V1, par exemple…
  timeline-q:
    q: Quand pourrais-je utiliser Mobilizon ?
    a:
      p:
      - Aujourd’hui, le code est encore en cours de développement, et ne permet
        pas une utilisation simple et sereine.
      - 'À l’automne 2019, nous prévoyons de sortir une version bêta, dont les fonctionnalités
        dépendront des sommes récoltées lors de la collecte. Cette version, qui
        équivaut à un premier jet, permettra un usage non garanti : la documentation
        sera en cours d’édition, et il restera probablement des améliorations à
        apporter.'
      - Notez que, si nous atteignons le 2<sup>e</sup> palier, nous ouvrirons au
        public une instance de démonstration, où chacun·e pourra aller cliquer et
        expérimenter, mais dont les données seront effacées régulièrement.
      - 'C’est au 1er semestre 2020, lors de la sortie de la version 1, que le logiciel
        Mobilizon sera aisément utilisable par le grand public. '
crowdfunding:
  title: Questions crowdfunding
  third-plateform:
    q: Pourquoi ne pas utiliser une plateforme comme GoFundMe ou KickStarter ?
    a:
      p:
      - Framasoft est une association domiciliée en France, ce qui nous prive d’accès
        à de nombreuses plateformes exigeant une domiciliation fiscale en Amérique
        du Nord. De plus, les plateformes françaises (que <a href="https://fr.ulule.com/etherpad-framapad/">nous
        avons déjà utilisées</a> à <a href="https://www.kisskissbankbank.com/fr/projects/peertube-a-free-and-federated-video-platform">plusieurs
        reprises</a>) ne proposent pas un outil qui se conforme exactement à la
        collecte que nous voulions faire cette fois-ci.
      - Nous avons donc décidé d’économiser sur les 5 % de commission que ces intermédiaires
        touchent en moyenne, et de créer notre propre site de collecte de fonds.
        Nous avions déjà les briques bancaires et comptables pour collecter des
        dons, il nous a donc suffit de créer l’interface web.
      - L’avantage d’avoir un outil fait maison, c’est que vos données ne transitent
        pas par un intermédiaire de plus. Comme pour l’ensemble des <a href="https://degooglisons-internet.org">services
        éthiques</a> que nous proposons depuis des années, ici aussi, <a href="https://framasoft.org/fr/cgu/">vos
        données ne nous intéressent pas</a>, car le respect de votre vie privée
        nous tient sincèrement à cœur.
  trust:
    q: Comme je sais si je peux vous faire confiance ?
    a:
      p:
      - C’est une question qu’il faut absolument se poser, car chacun·e a ses propres
        niveaux de confiance, qui dépendent de bien des choses. Ici, nous vous donnerons
        des éléments, après, c’est à vous de vous faire votre propre idée.
      - 'Framasoft est placée sous le régime français des associations à but non-lucratif :
        nous n’avons pas pour but de gagner de l’argent, juste celui d’avoir les
        moyens de poursuivre nos actions et d’arriver à l’équilibre financier. Ainsi,
        nous n’avons aucun intérêt à exploiter vos données, vous profiler, etc.'
      - Le modèle économique de Framasoft est celui du don, et ce depuis des années.
        Nos comptes sont d’ailleurs vérifiés par un commissaire aux comptes indépendant,
        dont <a href="https://framasoft.org/fr/association/">les rapports sont publiés
        ici</a>, en français (rapport 2018 en cours d’édition).
      - Nos actions sont connues et reconnues dans le milieu francophone du logiciel
        libre. De notre <a href="https://framalibre.org ">annuaire de ressources
        libres</a> à notre <a href="https://framabook.org">maison d’édition</a>
        de livres sous licences libres en passant par les <a href="https://degooglison-internet.org">34
        services alternatifs</a> que nous hébergeons pour Dégoogliser Internet,
        ce que nous avons réalisé est preuve d’une éthique forte.
      - Avec plus de 15 ans d’existence, la réputation de Framasoft est établie
        (cherchez-nous !). Nous avons déjà eu recours aux collectes pour financer
        les développements de <a href="https://fr.ulule.com/etherpad-framapad/">MyPads</a>
        et <a href="https://www.kisskissbankbank.com/fr/projects/peertube-a-free-and-federated-video-platform">PeerTube</a>.
        Et nous avons développé ces deux logiciels <a href="https://joinpeertube.org/fr/news/">au-delà
        de ce qui avait été promis</a> (et financé) lors de ces campagnes.
  noreward:
    q: Pourquoi ne proposez-vous pas de contreparties ?
    a:
      p1:
      - Parce que la seule vraie contrepartie ici, c’est la création d’un <em>commun</em>
        numérique. En effet, Mobilizon est <a href="https://framagit.org/framasoft/mobilizon/blob/master/LICENSE">sous
        licence libre</a>, ce qui signifie qu’il appartient à tout le monde, sans
        que personne ne puisse se l’approprier exclusivement.
      - Le système des contreparties des <i lang="fr">crowdfunding</i> classiques
        coûte beaucoup en temps, en argent et en énergie. Or nous faisons le pari
        que suffisamment de personnes seront prêtes à financer une alternative crédible
        aux événements Facebook, juste pour que tout le monde puisse, un jour, en
        profiter.
      - Enfin, nous ne voulons pas favoriser les contributions de tel ou tel en
        fonction de leurs revenus, car chacun·e a des moyens différents, et chacun·e
        peut participer, à sa mesure, à la naissance d’un commun numérique.
      - 'Ainsi, toute personne qui contribue à financer Mobilizon aura droit (si
        elle le désire) à :'
      ul:
      - son nom/pseudo dans le fichier <i lang="fr">readme</i> de Mobilizon et au
        tableau d’honneur de Joinmobilizon.org
      - recevoir dans sa boite mail la lettre d’informations de Mobilizon
  receipt:
    q: Aurais-je un reçu pour mon don ?
    a:
      p:
      - Oui, à condition que vous cochiez la case adéquate dans le formulaire de
        don et que vous nous fournissiez une adresse postale, ce qui nous est demandé
        légalement pour pouvoir émettre un reçu de dons.
      - Votre reçu sera automatiquement généré en mars 2020, et envoyé à l’adresse
        email que vous aurez renseignée.
      - Si vous avez besoin d’un reçu avant cette date, ou si vous changez d’adresse
        email, vous pouvez <a href="https://contact.framasoft.org">nous contacter
        ici</a>.
  taxes:
    q: Mon don sera-t-il déductible de mes impôts ?
    a:
      p:
      - Oui, pour les contribuables français.
      - Framasoft étant une association d’intérêt général, tout don peut donner
        droit à <a href="https://framablog.org/2018/11/22/impots-et-dons-a-framasoft-le-prelevement-a-la-source-en-2019/">une
        réduction de 66 %</a> sur vos impôts sur le revenu.
      - Ainsi, un don « entretien d’UX » à 130 € reviendrait, après déduction, à
        44 € 20 pour les contribuables français.
      - Si vous ne payez pas d’impôts sur le revenu en France, veuillez vous renseigner
        auprès des services fiscaux de votre pays.
